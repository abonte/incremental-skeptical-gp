import unittest
from unittest.mock import MagicMock, call

from sklearn.gaussian_process.kernels import RBF
import copy

from isgp.isgp import ISGP, ContradictionType

config = {
    "random_state": 0,
    "noise_level": 0.2,
    "kernel": 1.0 * RBF(2),
    "sigma_squared": 10e-10,
    "verbose": False,
    "contradiction_condition": None,
    "measure_run_time": False,
    "noise": 0.2
}


class TestWrapperSKL(unittest.TestCase):

    def test_add_to_dataset(self):
        wrapper = ISGP(config)
        x = [1, 2]
        y = 3
        wrapper.add_to_dataset([1, 2], 3)
        self.assertEqual(wrapper.xtrain, [x])
        self.assertEqual(wrapper.ytrain, [y])


class TestISGP(unittest.TestCase):

    def setUp(self) -> None:
        self.config = copy.deepcopy(config)

    def test_gp_never(self):
        self.config["contradiction_condition"] = ContradictionType.NEVER
        isgp = ISGP(self.config)
        self.assertTrue(isgp.accept_user_label(1, 1, 1))

    def test_gp_always(self):
        self.config["contradiction_condition"] = ContradictionType.ALWAYS
        isgp = ISGP(self.config)
        self.assertFalse(isgp.accept_user_label(1, 1, 1))

    def test_gp_stochastic_when_gamma_probability_is_one(self):
        self.config["contradiction_condition"] = ContradictionType.STOCHASTIC
        isgp = ISGP(self.config)
        isgp.mean_std_predictor = MagicMock(side_effect=[(-1, .1), (1, .1)])
        xt = [0.3, 0.5]
        y_predicted = 10
        yt = 20

        actual_value = isgp.accept_user_label(xt, y_predicted, yt)

        self.assertFalse(actual_value)
        isgp.mean_std_predictor.assert_has_calls([call(xt, yt), call(xt, y_predicted)])

    def test_gp_stochastic_when_gamma_probability_is_zero(self):
        self.config["contradiction_condition"] = ContradictionType.STOCHASTIC
        isgp = ISGP(self.config)
        isgp.mean_std_predictor = MagicMock(side_effect=[(1, 0.01), (-1, 0.01)])
        self.assertTrue(isgp.accept_user_label(1, 1, 1))

    def test_active_condition_when_return_true(self):
        isgp = ISGP(self.config)
        isgp.mean_std_predictor = MagicMock(side_effect=[(0, 1)])
        self.assertTrue(isgp.active_condition(1, 1))

    def test_active_condition_when_return_false(self):
        isgp = ISGP(self.config)
        isgp.mean_std_predictor = MagicMock(side_effect=[(1, 0.1)])
        self.assertFalse(isgp.active_condition(1, 1))

    def test_active_condition_return_true_when_input_ypred_is_none(self):
        isgp = ISGP(self.config)
        isgp.mean_std_predictor = MagicMock(side_effect=[(1, 0.1)])
        self.assertTrue(isgp.active_condition(1, None))


if __name__ == '__main__':
    unittest.main()
