import numpy as np
from sklearn.gaussian_process.kernels import ConstantKernel as C, RationalQuadratic, RBF
from sklearn.gaussian_process.kernels import Kernel, StationaryKernelMixin, NormalizedKernelMixin


class myKernel(StationaryKernelMixin, NormalizedKernelMixin, Kernel):

    def __init__(self, constant_value=1.0, length_scale_rq=0.2, length_scale_rbf=1.0, alpha=1.0):
        self.kernel1_ = RationalQuadratic(alpha=alpha, length_scale=length_scale_rq)
        self.kernel2_ = RBF(length_scale=length_scale_rbf)
        self.kernel3_ = C(constant_value=constant_value)

    def __call__(self, X1, X2=None, eval_gradient=False):
        X1 = np.atleast_2d(X1)
        if X2 is not None:
            #X2 = np.atleast_2d(X2)
            return self.kernel3_(X1, X2) * (self.kernel2_(X1[:, 0:-3], X2[:, 0:-3]) + self.kernel1_(X1[:, -3:], X2[:, -3:]))
        else:
            return self.kernel3_(X1) * (self.kernel2_(X1[:, 0:-3]) + self.kernel1_(X1[:, -3:]))

    def set_params(self, **params):
        self.kernel1_.set_params(**{'length_scale': params['length_scale_rq'],
                                     'alpha': params['alpha']
                                     })
        self.kernel2_.set_params(**{'length_scale': params['length_scale_rbf']})
        self.kernel3_.set_params(**{'constant_value': params['constant_value']})
        return self

    def get_params(self, deep=True):
        params = dict()
        par_k1 = self.kernel1_.get_params()
        par_k2 = self.kernel2_.get_params()
        par_k3 = self.kernel3_.get_params()
        params['length_scale_rq'] = par_k1['length_scale']
        params['alpha'] = par_k1['alpha']
        params['constant_value'] = par_k3['constant_value']
        params['length_scale_rbf'] = par_k2['length_scale']
        return params