# coding=utf-8
import numpy as np


def compute_Kprime_inv(kernel, K_inv, X_train, xi, sigma_squared):
    xi = np.atleast_2d(xi)
    if K_inv is None:
        K = kernel(X_train)
        K = K + sigma_squared * np.identity(K.shape[0])
        K_inv = np.linalg.inv(K)
    K_inv = _incrementSizeByOne(K_inv, 1)
    U, V = _getUV(kernel, X_train, xi, sigma_squared)
    Kprime_inv = woodburyFormula(K_inv, U, V)
    return Kprime_inv


def _getUV(kernel, X_train, xi,sigma_squared):
    ai = _computeAi(kernel, X_train, xi, sigma_squared)
    ei = _compute_ith_UnityVector(len(X_train)+1)
    U = _computeU(ai, ei)
    V = _computeV(ai, ei)
    return U, V


def _incrementSizeByOne(mat, value=1):
    mat = np.append(mat, np.zeros(mat.shape[0]).reshape(1, -1), axis=0)  # add 1 row at the end
    mat = np.append(mat, np.eye(1, mat.shape[0], mat.shape[0] - 1).T * value, axis=1) # add 1 column at the end
    return mat


def _computeAi(kernel, X_train, xi, sigma_squared):
    # compute the vector of differences between old and new kernel values for x_i
    a = kernel(X_train, xi)
    a = np.append(a, 0.5*(kernel(xi)+sigma_squared-1))
    return a


def _compute_ith_UnityVector(index):
    return np.append(np.zeros(index - 1), 1)


def _computeU(a, e):
    return np.stack((a, e), axis=-1)


def _computeV(a, e):
    return np.stack((e, a), axis=0)


def woodburyFormula(K_inv, U, V):
    C_inv = np.identity(2)
    B = np.dot(K_inv, U)
    temp = C_inv + np.dot(V, B)  # C_inv + V K_inv U
    temp_inv = np.linalg.inv(temp)
    subtrahend = np.dot(np.dot(np.dot(B, temp_inv), V), K_inv)  # K_inv U (C_inv + V K_inv U) V K_inv
    Kprime_inv = K_inv - subtrahend
    return Kprime_inv
