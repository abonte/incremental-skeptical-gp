# coding=utf-8
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_is_fitted, check_array, check_X_y
from sklearn.utils.multiclass import check_classification_targets
from sklearn.exceptions import NotFittedError
import numpy as np
import random
from .WoodburyFormula import compute_Kprime_inv
from .myKernel import myKernel


class IncrementalGaussianProcessClassifier(BaseEstimator, ClassifierMixin):
    """Gaussian process classification based on an efficient incremental learning method.

    The implementation is based on [lutz2013want].

    The classifier is scikit-learn compatible.

    The current implementation is restricted to iteratively incrementing one example.
    It does not support the multi-class classification.

    [lutz2013want] Lütz, Alexander, Erik Rodner, and Joachim Denzler.
                    "I want to know more—efficient multi-class incremental learning using Gaussian processes."
                    Pattern recognition and image analysis 23.3 (2013): 402-407.

    Parameters
    ----------
    kernel : kernel object
        The kernel specifying the covariance function of the GP. If None is
        passed, the kernel "RationalQuadratic(alpha=1.0, length_scale=0.2)" is used as default.
        Note that the kernel's hyperparameters are not optimized during fitting.

    sigma_squared : float, (default: 0.1)
        Variance of the Gaussian noise in the observed values.

    Attributes
    ----------
    kernel_ : kernel object
        The kernel used for prediction.
    classes_ : array-like, shape = (n_classes,)
        Unique class labels.
    n_classes_ : int
        The number of classes in the training data.
    X_train_ : array-like, shape = (n_samples, n_features)
        All training data
    y_train_ : array-like, shape = (n_samples,)
        All target values
    Kprime_inv_ : array-like, shape = (n_samples, n_samples)
        The inverse of the regularized kernel matrix (􏰍K + sigma^2_n I􏰎)
    """

    def __init__(self, kernel=None, sigma_squared=1e-10):
        self.sigma_squared = sigma_squared
        self.kernel = kernel

    def fit(self, X, y):
        """Fit Gaussian process classification model.
        It deletes previous learned models.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data

        y : array-like, shape = (n_samples,)
            Target values

        Returns
        -------
        self : returns an instance of self.
        """
        return self._partial_fit(X, y, _refit=True)

    def partial_fit(self, X, y):
        """ Efficient incremental learning of the Gaussian process classification model

        If a new training sample becomes available, it is possible to update the learned model.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data

        y : array-like, shape = (n_samples,)
            Target values

        Returns
        -------
        self : returns an instance of self.
        """

        return self._partial_fit(X, y, _refit=False)

    def predict(self, X):
        """Perform classification on an array of test vectors X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        C : array, shape = (n_samples,)
            Predicted target values for X, values are from ``classes_``
        """
        self._check_is_fitted()

        y = np.array([])
        X = check_array(X)

        if X.shape[0] > 1:
            for x in X:
                x = np.atleast_2d(x)
                index = self._predict(x)
                y = np.append(y, self.classes_[index])
        else:
            index = self._predict(X)
            y = np.append(y, self.classes_[index])
        return y

    def predict_proba(self, X):
        """Return score estimates for the test vector X.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)

        Returns
        -------
        C : array-like, shape = (n_samples, n_classes)
            Returns the score of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute `classes_`.
        """
        self._check_is_fitted()

        y = np.array([]).reshape(0, self.n_classes_)
        X = check_array(X)

        if X.shape[0] > 1:
            for x in X:
                x = np.atleast_2d(x)
                result = self._predict_mean(x)
                y = np.vstack((y, result))
        else:
            result = self._predict_mean(X)
            return np.atleast_2d(result)
        return y

    def _partial_fit(self, X, y, _refit=False):
        y = np.atleast_1d(y)
        X = np.atleast_2d(X)
        X, y = check_X_y(X, y, multi_output=False)
        check_classification_targets(y)
        if self.kernel is None:
            self.kernel = myKernel()
            # self.kernel = RationalQuadratic(alpha=1.0, length_scale=0.2)

        if _refit or not hasattr(self, 'X_train_'):
            self.X_train_ = None
            self.y_train_ = np.array([], dtype=int)
            self.Kprime_inv_ = None
            self.binary_label = np.array([1], dtype=int).reshape(1, -1)
            self.classes_ = np.array([], dtype=int)

        if X.shape[0] > 1:
            for data, target in zip(X, y):
                self._add_sample(data, target)
        else:
            self._add_sample(X, y)
        return self

    def _add_sample(self, x, y):
        if self.X_train_ is not None:
            self.Kprime_inv_ = compute_Kprime_inv(self.kernel, self.Kprime_inv_, self.X_train_, x, self.sigma_squared)
            self.X_train_ = np.vstack([self.X_train_, x])
        else:
            self.X_train_ = np.atleast_2d(x)

        self.y_train_ = np.append(self.y_train_, y)
        self.binary_label = self._label_binarizer(y)
        # self.classes_, i = np.unique(self.classes_, return_index=True)
        # tmp = self.binary_label.T[i]
        # self.binary_label = tmp.T
        self.n_classes_ = self.classes_.size
        if self.Kprime_inv_ is not None:
            self.alpha = np.empty((0, self.X_train_.shape[0]))
            for tl in self.binary_label.T:
                self.alpha = np.vstack([self.alpha, self.Kprime_inv_.dot(tl)])
            self.alpha = np.atleast_2d(self.alpha)

    def _predict(self, x_star):
        mean_predictions_by_labels = self._predict_mean(x_star)
        if len(set(mean_predictions_by_labels)) == 1:
            return random.randint(0, self.binary_label.shape[1]-1)
        else:
            return mean_predictions_by_labels.argmax()

    def _predict_mean(self, new_x):
        # Return mean estimation for the test vector new_x.
        k_star = self.kernel(self.X_train_, new_x).T
        return np.array([k_star.dot(tl.T)[0] for tl in self.alpha])

    def compute_variance(self, x_star):
        self._check_is_fitted()
        x_star = np.atleast_2d(x_star)
        k_star = self.kernel(self.X_train_, x_star)
        k_star_star = self.kernel(x_star)
        variance = k_star_star - k_star.T.dot(self.Kprime_inv_).dot(k_star) + self.sigma_squared  # eq (3)
        return variance[0][0]

    def _label_binarizer(self, y):
        if y not in self.classes_:
            self.classes_ = np.append(self.classes_, y)
            if len(self.classes_) == 1:
                return self.binary_label
            self.binary_label = np.c_[self.binary_label, np.full(self.binary_label.shape[0], -1, dtype=int)]
            index = self.classes_.size - 1
        else:
            index = np.where(self.classes_ == y)[0][0]
        a = np.full(self.classes_.size, -1, dtype=int)
        a[index] = 1
        return np.vstack([self.binary_label, a])

    def _check_is_fitted(self):
        check_is_fitted(self, ["classes_", "n_classes_"],
                        msg="This %(name)s instance is not fitted yet. Call 'fit' with at least two samples or "
                            "'incremental_learning' at least two times before using this method.")
        if self.Kprime_inv_ is None:
            raise NotFittedError(" %(name)s requires at least two samples; got {0:d}".format(len(self.X_train_)))