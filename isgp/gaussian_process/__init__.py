from .GPIncrementalLearning import IncrementalGaussianProcessClassifier

__all__ = ['IncrementalGaussianProcessClassifier']