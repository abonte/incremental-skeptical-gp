# coding=utf-8
import logging
import time
from enum import Enum, auto
from math import sqrt

import numpy as np
from scipy import stats
from sklearn.exceptions import NotFittedError

from .gaussian_process import IncrementalGaussianProcessClassifier
from .wrapper_skl import WrapperSKL


class ContradictionType(Enum):
    ALWAYS = auto()
    NEVER = auto()
    STOCHASTIC = auto()


class ISGP(WrapperSKL):

    def __init__(self, config: dict):
        super().__init__(config)
        self.classifier = IncrementalGaussianProcessClassifier(kernel=config['kernel'], sigma_squared=config[
            'sigma_squared'])

    def one_step(self, xt, n_iteration: int = None) -> None:
        y_predicted = self.predict(xt, first_class=None)

        logging.debug(f'y_predicted: {y_predicted}')

        if self.active_condition(xt, y_predicted):
            yt = self.ask_user(xt)
            logging.debug(f'y_user {yt}')
            if y_predicted is None:
                self.train_machine(self._get_data_part_from_xt(xt), yt)
            else:
                self.solve_conflict(xt, y_predicted, yt)

    def active_condition(self, xt, y_predicted) -> bool:
        # Eq. 7
        if y_predicted is None:
            return True
        try:
            mean, std = self.mean_std_predictor(xt, y_predicted)
        except NotFittedError:
            return True

        alpha_probability = stats.norm.cdf(mean / std)
        return np.random.binomial(1, 1 - alpha_probability)

    def solve_conflict(self, xt, y_predicted, yt) -> None:

        if y_predicted == yt:
            y_prime = y_predicted
        else:
            if self.accept_user_label(xt, y_predicted, yt):
                logging.debug('[user ground truth]')
                y_prime = yt
            else:
                y_prime = self.challenge_user(xt, yt, y_predicted)
                logging.debug(f'[contradiction] y_prime = {y_prime}')

        self.add_to_dataset(self._get_data_part_from_xt(xt), y_prime)
        self.train_machine(self._get_data_part_from_xt(xt), y_prime)

    def accept_user_label(self, xt, y_predicted, yt) -> bool:
        if self.config["contradiction_condition"] == ContradictionType.NEVER:
            return True
        elif self.config["contradiction_condition"] == ContradictionType.ALWAYS:
            return False
        elif self.config["contradiction_condition"] == ContradictionType.STOCHASTIC:
            try:
                mean_user_label, std = self.mean_std_predictor(xt, yt)
                mean_machine_label, _ = self.mean_std_predictor(xt, y_predicted)
                gamma_probability = stats.norm.cdf((mean_machine_label - mean_user_label) / std)  # Eq. 8
                return np.random.binomial(1, 1 - gamma_probability)

            except NotFittedError:
                return True

    def train_machine(self, x, y) -> None:
        t0 = time.process_time()
        self.classifier.partial_fit(x, y)
        t1 = time.process_time()
        self.statistics["training_times"].append(t1 - t0)

    def iterate_over_test_data(self, test_data):
        y_oracle, y_machine = [], []
        for _, row in test_data.iterrows():
            y_oracle.append(self.get_oracle_label(row))
            y_machine.append(self.predict(row))

        return y_oracle, y_machine

    def mean_std_predictor(self, xt, y):
        xt_tmp = self._get_data_part_from_xt(xt)
        machine_std = sqrt(self.classifier.compute_variance(xt_tmp.values.reshape(1, -1)))
        try:
            proba = self.classifier.predict_proba(xt_tmp.values.reshape(1, -1))[0]
            index = np.where(self.classifier.classes_ == y)[0]
            machine_mean = proba[index[0]] if index.size != 0 else 0
        except NotFittedError:
            machine_mean = float(1) / self.classifier.n_classes_ if hasattr(self.classifier, "classes_") else 0

        return machine_mean, machine_std
