# coding=utf-8
import copy
import logging
import time
from abc import abstractmethod, ABC

import numpy as np
from sklearn.exceptions import NotFittedError
from sklearn.metrics import f1_score


class WrapperSKL(ABC):

    def __init__(self, config: dict):
        self.config = config
        self.classifier = None
        self.statistics = {
            "active_queries": [],
            "contradiction_queries": [],
            "correct_contradictions": [],
            "f1_scores": [],
            "training_times": [],
            "predicting_times": [],
            "likelihood": np.array([]),
            "classifier": None,
            "xtrain": None,
            "ytrain": None
        }

        self.xtrain = []
        self.ytrain = []

        self.n_active_queries = 0
        self.n_contradiction_queries = 0
        self.n_correct_contradictions = 0

        level = logging.DEBUG if config["verbose"] else logging.INFO
        FORMAT = "%(asctime)s %(message)s"
        logging.basicConfig(level=level, format=FORMAT, datefmt='%H:%M:%S')
        logging.getLogger(__name__).addHandler(logging.NullHandler())
        np.random.seed(config["random_state"])

    def run(self, training_data, testing_data) -> dict:
        logging.info("Start iterations")

        self.iterate_over_data(training_data, testing_data)

        logging.info(f"Input finished. Active queries {str(self.statistics['active_queries'][-1])}")

        self.statistics["classifier"] = copy.deepcopy(self.classifier)
        self.statistics["xtrain"] = self.xtrain
        self.statistics["ytrain"] = self.ytrain
        return self.statistics

    def iterate_over_data(self, training_data, testing_data) -> None:
        for n_iteration, (_, xt) in enumerate(training_data.iterrows()):

            logging.debug(f'===={n_iteration}/{len(training_data)}====')

            if not self.config["measure_run_time"]:
                self.one_step(xt, n_iteration)
            else:
                self.predict(xt, measure_time=True, first_class=self.get_oracle_label(xt))
                self.train_machine(self._get_data_part_from_xt(xt), self.get_oracle_label(xt))

            self.compute_statistics(testing_data)

    def compute_statistics(self, testing_data):
        self.statistics['active_queries'].append(self.n_active_queries)
        self.statistics["contradiction_queries"].append(self.n_contradiction_queries)
        self.statistics['correct_contradictions'].append(self.n_correct_contradictions)
        self.test_model(testing_data)

    @abstractmethod
    def one_step(self, xt, n_iteration=None):
        raise NotImplementedError("Must override")

    def ask_user(self, xt):
        self.n_active_queries += 1
        return self.get_user_label(xt)

    def add_to_dataset(self, x, y):
        self.xtrain.append(x)
        self.ytrain.append(y)

    def challenge_user(self, xt, yt, y_predicted):
        self.n_contradiction_queries += 1

        if self.get_oracle_label(xt) == yt:
            # return correct class
            label_to_return = self.get_oracle_label(xt)
        else:
            if np.random.binomial(1, self.config['noise_level']):
                # return random class
                classes = np.arange(6, dtype=float)
                label_to_return = np.random.choice(classes[classes != yt])
            else:
                label_to_return = self.get_oracle_label(xt)

        if self.get_oracle_label(xt) == y_predicted:
            self.n_correct_contradictions += 1

        return label_to_return

    @abstractmethod
    def train_machine(self, x, y) -> None:
        raise NotImplementedError("Must override")

    def predict(self, xt, measure_time=False, first_class=None):
        try:
            t0 = time.process_time()
            y_predicted = self.classifier.predict(self._get_data_part_from_xt(xt).values.reshape(1, -1))[0]
            t1 = time.process_time()
            if measure_time:
                self.statistics['predicting_times'].append(t1 - t0)
            return y_predicted
        except NotFittedError:
            if measure_time:
                self.statistics['predicting_times'].append(0)
            if not hasattr(self.classifier, "classes_"):
                return first_class
            else:
                return self.classifier.classes_[0]

    def test_model(self, test_data) -> None:
        y_objective, y_predicted = self.iterate_over_test_data(test_data)
        self.statistics['f1_scores'].append(f1_score(y_objective, y_predicted, average='weighted'))

    @abstractmethod
    def iterate_over_test_data(self, test_data):
        raise NotImplementedError("Must override")

    @staticmethod
    def _get_data_part_from_xt(xt):
        return xt[['x', 'y']]

    @staticmethod
    def get_oracle_label(xt):
        return xt['oracle']

    def get_user_label(self, xt):
        return xt[str(self.config['noise'])]
