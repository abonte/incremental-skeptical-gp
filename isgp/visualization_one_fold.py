import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.lines as mlines
import os
from matplotlib.lines import Line2D


class Visualization:
    fig_num = 0
    n_plot_row = 0
    n_plot_col = 0
    _ccmapcolor = ['Reds', 'Blues', 'Oranges', 'Purples', 'Greys', 'Greens']
    _colors = ['red', 'blue', 'orange', 'purple', 'black', 'green']

    def __init__(self, timestamp, active=True, n_plot_row=1, n_plot_col=1, dataset=None, grid=True, directory=None):
        self.n_plot_col = n_plot_col
        self.n_plot_row = n_plot_row
        plt.set_loglevel("info")
        plt.figure(figsize=(7 * self.n_plot_col, 7 * self.n_plot_row))
        self.xx, self.yy = self.create_meshgrid(dataset)
        self._grid = grid
        self._timestamp = timestamp
        self._active = active
        if directory is not None:
            self._directory_plots = directory
        if not os.path.exists(self._directory_plots):
            os.makedirs(self._directory_plots)

    def target_to_color(self, target):
        return np.array(list(map(lambda x: self._colors[int(x)], target)))

    @staticmethod
    def create_meshgrid(dataset):
        h = .2  # step size in the mesh
        x_min, x_max = dataset['x'].min() - .5, dataset['y'].max() + .5
        y_min, y_max = dataset['x'].min() - .5, dataset['y'].max() + .5
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        return xx, yy

    def plot_input_data(self, training_data, noise):
        ax = self.get_ax()

        ax.set_title("Input data")
        ax.set_ylabel("y")
        ax.set_xlabel("x")

        plt.scatter(training_data['x'], training_data['y'],
                    c=self.target_to_color(training_data[str(noise)]),
                    marker='x')

        self.show("input_data")

    def contour_plot(self, stat, training_data, testing_data, noise, name):
        noise = str(noise)
        ax = self._plot_contour(stat=stat, noise_level=noise, name=name)
        self._scatter_plot_points(ax, stat, training_data, testing_data, noise, name + noise)

    def _plot_contour(self, stat, noise_level, name):
        ZZ = stat['classifier'].predict_proba(np.c_[self.xx.ravel(), self.yy.ravel()])
        ax = self.get_ax()
        ax.set_title(self.get_name_clf(name) + ", noise level=" + str(noise_level))
        ax.set_ylabel("y")
        ax.set_xlabel("x")
        ZZ = stat['classifier'].predict(np.c_[self.xx.ravel(), self.yy.ravel()])
        for index, clazz in enumerate(stat['classifier'].classes_):
            clazz = int(clazz)

            # tmpZ = ZZ[:, index].reshape(self.xx.shape)

            ccc = self.target_to_color(ZZ)

            ax.scatter(self.xx, self.yy, color=ccc, alpha=.01, marker='s')

            #
            # ax.contour(self.xx, self.yy, tmpZ,
            #            alpha=.9,
            #            levels=[0.2, 0.3],
            #            linestyles=['dotted', 'solid'],
            #            colors=self._colors[clazz])

        patch1 = mlines.Line2D([], [], color='black', label='0.3')
        patch2 = mlines.Line2D([], [], color='black', label='0.2', linestyle=":")
        ax.legend(handles=[patch1, patch2])
        return ax

    def _scatter_plot_points(self, ax, stat, training_data, testing_data, noise, name):
        dat = pd.DataFrame(stat['xtrain'])
        trained = np.array(stat['ytrain'], dtype=int)
        actual_target_class = self.target_to_color(trained)
        if not self._active:
            indexes = np.where((trained == training_data[str(noise)]) == False)[0]
            # tmp_dat = dat.drop(indexes)
            # indexes_1 = np.where((trained == training_data['oracle']) == False)[0]
            # ax.scatter(dat["x"].iloc[indexes_1], dat["y"].iloc[indexes_1], c=actual_target_class[indexes_1], marker='x')
            # ax.scatter(tmp_dat["x"], tmp_dat["y"], c=np.delete(actual_target_class, indexes), marker='x')
            ax.scatter(dat["x"].iloc[indexes], dat["y"].iloc[indexes], c=actual_target_class[indexes], marker='s')
            ax.scatter(testing_data["x"], testing_data["y"], c=self.target_to_color(testing_data['oracle']), marker="^")
        else:
            ax.scatter(dat["x"], dat["y"],
                       c=actual_target_class,
                       marker='x')

            ax.scatter(testing_data["x"], testing_data["y"],
                       c=self.target_to_color(testing_data['oracle']),
                       marker="^")

        # modeline = clf.mode_Line
        # for i in range(1, len(modeline)):
        #     if modeline[i][0] == "Training":
        #         nn = modeline[i][1]
        # ind = np.where((training_data['oracle'] != training_data['lb']) == True)[0]
        # for i, r in training_data.iloc[ind].iterrows():
        #    ax.annotate(str(i), (r['x'], r['y']))

        self.show(name)

    def plot_queries(self, all_stat):
        ax = self.get_ax()
        ax.set_title("Number of user contraddiction and the machine is right")
        ax.set_xlabel("Iterations")
        ax.set_ylabel("#")
        custom_lines = list()
        custom_labels = list()
        for i, (k, stat) in enumerate(all_stat.items()):
            color = self.get_color_clf(k)
            custom_lines.append(Line2D([0], [0], color=color, lw=2))
            custom_labels.append(self.get_name_clf(k))
            if i == 0:
                custom_lines.append(Line2D([0], [0], color="black", linestyle="--", lw=2))
                custom_labels.append("machine is correct")
                custom_lines.append(Line2D([0], [0], color="black", lw=2))
                custom_labels.append("user contradiction")

            ax.plot(np.arange(len(stat['correct_contradictions'])), stat['correct_contradictions'],
                    label=k + ": # machine is correct",
                    color=color,
                    linestyle="--")
            ax.plot(np.arange(len(stat['contradiction_queries'])), stat['contradiction_queries'],
                    label=k + ": # contradiction queries",
                    color=color)

            if self._active:
                ax.plot(np.arange(len(stat['active_queries'])), stat['active_queries'], label=k + ": # first queries",
                        color=color,
                        linestyle="-.")
                if i == 0:
                    custom_lines.append(Line2D([0], [0], color="black", linestyle="-.", lw=2))
                    custom_labels.append("user queries")

        ax.legend(custom_lines, custom_labels, loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=1)
        ax.grid()
        self.show("n_queries")

    def plot_f1_score(self, all_stat):
        ax = self.get_ax()
        ax.set_title("F1 score at each iteration")
        ax.set_xlabel("Iterations")
        ax.set_ylabel("#")
        for k, stat in all_stat.items():
            x = np.arange(len(stat['f1_scores']))
            ax.plot(x, stat['f1_scores'], label=k, color=self.get_color_clf(k))
        ax.legend()
        ax.grid()
        self.show("f1_score")

    def plot_training_predicting_times(self, all_clf, clf_names):
        if not self._active:
            self._plot_times(all_clf, clf_names, "train")
            self._plot_times(all_clf, clf_names, "predict")

    def _plot_times(self, all_clf, clf_names, type):
        ax = self.get_ax()
        for name, clf in all_clf.items():
            if name in clf_names:
                if type == "train":
                    ax.plot(list(map(lambda x: x, clf.statistics.training_times)),
                            color=self.get_color_clf(name),
                            label=self.get_name_clf(name))
                elif type == "predict":
                    ax.plot(list(map(lambda x: x, clf.statistics.predicting_time)),
                            color=self.get_color_clf(name),
                            label=self.get_name_clf(name))

        ax.set_title("Time for " + type + "ing")
        ax.set_xlabel('Iterations')
        ax.set_ylabel(''.join(["time for ", str(type), "ing [s]"]))

        ax.grid()
        ax.legend()

        self.show("training_predicting")

    def get_ax(self, figsize=None):
        if self._grid:
            self.fig_num += 1
            ax = plt.subplot(self.n_plot_row, self.n_plot_col, self.fig_num)
            return ax
        else:
            plt.clf()
            plt.figure(num=None, figsize=figsize, dpi=150)
            return plt.gca()

    def show(self, figname):
        if not self._grid:
            self._save(figname)

    def show_and_save(self, experiment_name):
        if self._grid:
            self._save(experiment_name)

    def _save(self, figname):
        plt.tight_layout()
        plt.savefig(os.path.join(self._directory_plots, figname+'.png'), bbox_inches='tight', pad_inches=0.01)
        plt.close()

    @staticmethod
    def get_name_clf(name):
        if name == "GP_never":
            return r'$\mathrm{GP}_{never}$'
        elif name == "GP_always":
            return r'$\mathrm{GP}_{always}$'
        elif name == "ISGP":
            return "ISGP"
        elif name == "SRF":
            return "SRF"

    @staticmethod
    def get_color_clf(name):
        if name == "GP_never":
            return '#8ae234'
        elif name == "GP_always":
            return '#4e9a06'
        elif name == "ISGP":
            return '#ef2929'
        elif name == "SRF":
            return '#204a87'
