import numpy as np
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import os


class VisualizationAverage:
    fig_num = 0
    figsize = (7, 6)
    errorevery = 50
    capsize = 4
    linewidth = 2
    fontsize = 14
    capthick = 1
    elinewidth = linewidth - 1
    legend = True

    def __init__(self, active=True, n_plot_row=1, n_plot_col=1, grid=True, directory=None):
        self.n_plot_col = n_plot_col
        self.n_plot_row = n_plot_row
        plt.set_loglevel("info")
        plt.figure(figsize=(self.figsize[0] * self.n_plot_col, self.figsize[1] * self.n_plot_row))
        self._grid = grid
        self._active = active
        if directory is not None:
            self._directory_plots = directory
        if not os.path.exists(self._directory_plots):
            os.makedirs(self._directory_plots)

    def plot_queries_folds(self, figname, mean, std, experiments_args):
        ax = self.get_ax(self.figsize)
        # ax.set_title("Number of queries, noise = " + str(experiments_args.noise), fontsize=self.fontsize)
        ax.set_xlabel("Iterations", fontsize=self.fontsize)
        ax.set_ylabel("#", fontsize=self.fontsize)

        custom_lines = [
            Line2D([0], [0], color="black", lw=2),
            Line2D([0], [0], color="black", linestyle="--", lw=2),
            Line2D([0], [0], color="black", linestyle="-.", lw=2)
        ]
        custom_labels = [
            "contradiction queries",
            "correct contradictions",
            "active queries"
        ]

        for method_name in mean.keys():
            custom_lines.append(Line2D([0], [0], color=self.get_color_clf(method_name), lw=self.linewidth))
            custom_labels.append(self.get_name_clf(method_name))
            x = np.arange(len(mean[method_name]['contradiction_queries']))

            ax.errorbar(x, mean[method_name]['contradiction_queries'], std[method_name]['contradiction_queries'],
                        errorevery=self.errorevery,
                        capsize=self.capsize,
                        color=self.get_color_clf(method_name),
                        linewidth=self.linewidth,
                        capthick=self.capthick,
                        elinewidth=self.elinewidth,
                        )

            ax.errorbar(x, mean[method_name]['correct_contradictions'], std[method_name]['correct_contradictions'],
                        errorevery=self.errorevery,
                        capsize=self.capsize,
                        color=self.get_color_clf(method_name),
                        linestyle="--",
                        linewidth=self.linewidth,
                        capthick=self.capthick,
                        elinewidth=self.elinewidth
                        )

            if self._active:
                ax.errorbar(x, mean[method_name]['active_queries'], std[method_name]['active_queries'],
                            errorevery=self.errorevery,
                            capsize=self.capsize,
                            color=self.get_color_clf(method_name),
                            linestyle="-.",
                            linewidth=self.linewidth,
                            capthick=self.capthick,
                            elinewidth=self.elinewidth,
                            label=self.get_name_clf(method_name) + ": user queries"
                            )

        ax.set_ylim(bottom=-2, top=55)
        ax.tick_params(axis="x", labelsize=self.fontsize)
        ax.tick_params(axis="y", labelsize=self.fontsize)

        # ax.legend(custom_lines, custom_labels, fontsize=self.fontsize - 2)
        ax.legend(custom_lines, custom_labels, bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                  ncol=2, mode="expand", borderaxespad=0., fontsize=self.fontsize - 2)

        ax.grid()
        self.show(figname + "_n_queries")

    def plot_f1_score_average(self, figname, mean, std, experiments_args):
        ax = self.get_ax(self.figsize)
        ax.set_title("F1 score, noise = " + str(experiments_args.noise), fontsize=self.fontsize)
        ax.set_xlabel("Iterations", fontsize=self.fontsize)
        ax.set_ylabel("F1 score", fontsize=self.fontsize)
        for method_name in mean.keys():
            x = np.arange(len(mean[method_name]['f1_scores']))
            ax.errorbar(x, mean[method_name]['f1_scores'], std[method_name]['f1_scores'],
                        errorevery=self.errorevery,
                        capsize=self.capsize,
                        label=self.get_name_clf(method_name),
                        color=self.get_color_clf(method_name),
                        linewidth=self.linewidth,
                        capthick=self.capthick,
                        elinewidth=self.elinewidth
                        )
        ax.set_ylim(bottom=-0.02, top=0.92)
        ax.set_yticks(np.arange(0, .92, step=0.1))
        ax.tick_params(axis="x", labelsize=self.fontsize)
        ax.tick_params(axis="y", labelsize=self.fontsize)

        ax.legend(fontsize=self.fontsize - 2)
        # ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
        # ncol=2, mode="expand", borderaxespad=0., fontsize=self.fontsize)

        ax.grid()
        self.show(figname + "_f1_score")

    def plot_training_predicting_times(self, mean, std):
        self._plot_times(mean, std, 'training_times', "train")
        self._plot_times(mean, std, 'predicting_times', "predict")

    def _plot_times(self, mean, std, metric_name, type):
        ax = self.get_ax(self.figsize)
        for method_name in mean.keys():
            x = np.arange(len(mean[method_name][metric_name]))

            ax.plot(x, mean[method_name][metric_name],
                    label=self.get_name_clf(method_name),
                    color=self.get_color_clf(method_name),
                    linewidth=self.linewidth,
                    )

        ax.set_title("Time for " + type + "ing", fontsize=self.fontsize)
        ax.tick_params(axis="x", labelsize=self.fontsize)
        ax.tick_params(axis="y", labelsize=self.fontsize)
        ax.set_xlabel('Iterations', fontsize=self.fontsize)
        ax.set_ylabel(''.join(["time for ", str(type), "ing [s]"]), fontsize=self.fontsize)
        ax.legend(fontsize=self.fontsize)
        self.show(type)

    def get_ax(self, figsize=None):
        if self._grid:
            self.fig_num += 1
            ax = plt.subplot(self.n_plot_row, self.n_plot_col, self.fig_num)
            return ax
        else:
            plt.clf()
            plt.figure(num=None, figsize=figsize, dpi=150)
            return plt.gca()

    def show(self, figname):
        if not self._grid:
            self._save(figname)

    def show_and_save(self, experiment_name):
        if self._grid:
            self._save(experiment_name)

    def _save(self, figname):
        plt.tight_layout()
        plt.savefig(os.path.join(self._directory_plots, figname + '.png'), bbox_inches='tight', pad_inches=0.01)
        plt.close()

    @staticmethod
    def get_name_clf(name):
        if name == "GP_never":
            return r'$\mathrm{GP}_{never}$'
        elif name == "GP_always":
            return r'$\mathrm{GP}_{always}$'
        elif name == "ISGP":
            return "ISGP"
        elif name == "SRF":
            return "SRF"

    @staticmethod
    def get_color_clf(name):
        if name == "GP_never":
            return '#8ae234'
        elif name == "GP_always":
            return '#4e9a06'
        elif name == "ISGP":
            return '#ef2929'
        elif name == "SRF":
            return '#204a87'
