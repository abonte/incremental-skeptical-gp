# coding=utf-8
import logging
import time
from collections import Counter
from collections import deque

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.exceptions import NotFittedError

from .wrapper_skl import WrapperSKL


class OriginalSKL(WrapperSKL):

    def __init__(self, config: dict):

        super().__init__(config)
        self.classifier = RandomForestClassifier(criterion='gini',
                                                 n_estimators=self.config['n_tree'],
                                                 random_state=self.config['random_state'])

        self.cu = None
        self.cm = None
        self.classes = None
        self.qm = deque(maxlen=self.config['d'])
        self.qu = deque(maxlen=self.config['d'])

        self.trainModeCondition = True
        self.refineModeCondition = True
        self.useModeCondition = True

        self.trainModeQueue = []
        self.refineModeQueue = []

    def run(self, training_data, testing_data):
        self.classes = training_data.oracle.unique()
        self.cu = self.initialize_confidence(self.classes, 1)
        self.cm = self.initialize_confidence(self.classes, 0.)
        return super().run(training_data, testing_data)

    def one_step(self, xt, n_iteration):
        if self.config['force_stage']:
            self.force_steps(xt, n_iteration)
        else:
            self.normal_steps(xt)

    def force_steps(self, xt, n_iteration):
        if n_iteration <= 5:
            logging.debug('Train mode:')
            self.train_stage(xt)
        elif 5 < n_iteration <= 10:
            logging.debug('Refine mode:')
            self.refine_stage(xt)
        else:
            logging.debug('Use mode: ')
            self.use_stage(xt)

    def normal_steps(self, xt):
        if self.trainModeCondition:
            logging.debug('Train mode:')
            self.train_stage(xt)
        elif self.refineModeCondition:
            logging.debug('Refine mode:')
            self.refine_stage(xt)
        else:
            logging.debug('Use mode: ')
            self.use_stage(xt)

    def train_stage(self, xt):
        yt = self.ask_user(xt)
        y_predicted = self.predict(xt, first_class=yt)
        logging.debug(f'yt: {yt} y_predicted: {y_predicted}')

        self.train_mode_value(self._get_data_part_from_xt(xt), yt, y_predicted)
        self.refine_mode_value(self._get_data_part_from_xt(xt), yt, y_predicted)

        self.train_machine(self._get_data_part_from_xt(xt), yt)
        self.cm, self.qm = self.update(self.cm, y_predicted, yt, self.qm)

        self.trainModeCondition = self.train_mode(self.calculate_bootstrap(self.ytrain))

    def refine_stage(self, xt):
        yt = self.ask_user(xt)
        y_predicted = self.predict(xt, measure_time=False)
        logging.debug(f'yt: {yt} y_predicted: {y_predicted}')

        self.refine_mode_value(self._get_data_part_from_xt(xt), yt, y_predicted)

        self.solve_conflict(xt, y_predicted, yt)

        self.refineModeCondition = self.refine_mode()

    def use_stage(self, xt):
        y_predicted = self.predict(xt, measure_time=False)
        logging.debug(f'y_predicted: {y_predicted}')

        predicted_confidence = self.predict_confidence(self.classifier, self._get_data_part_from_xt(xt), y_predicted)
        logging.debug(f'cm_y {self.cm[y_predicted]} * '
                      f'conf {predicted_confidence} = '
                      f'{self.cm[y_predicted] * predicted_confidence} < {self.config["theta"]} theta_q')

        if self.cm[y_predicted] * predicted_confidence < self.config['theta']:
            yt = self.ask_user(xt)
            self.solve_conflict(xt, y_predicted, yt)

    def train_mode_value(self, xt, yt, y_predicted):
        if len(self.trainModeQueue) == self.config['d']:
            # remove the older
            del self.trainModeQueue[:1]

        predicted_confidence = self.predict_confidence(self.classifier, xt, y_predicted)

        theta = self.config['theta']
        logging.debug(f'train mode value:  cm_y * conf {self.cm[y_predicted] * predicted_confidence} > '
                      f'{self.cu[yt] * theta} cu_y * theta_c')

        if self.cm[y_predicted] * predicted_confidence > self.cu[yt] * theta:
            self.trainModeQueue.append(1)
        else:
            self.trainModeQueue.append(0)

    def train_mode(self, bootstrap) -> bool:
        if bootstrap:
            return True
        else:
            theta = self.config['theta'] / 2
            train_mode_condition = float(self.trainModeQueue.count(1)) / len(self.trainModeQueue) <= theta
            logging.debug(f'train mode condition: {float(self.trainModeQueue.count(1)) / len(self.trainModeQueue)} '
                          f'<= {theta} theta_r ---> {train_mode_condition}')

            return train_mode_condition

    def refine_mode_value(self, xt, yt, ypredicted):
        if len(self.refineModeQueue) == self.config['d']:
            # remove the older
            del self.refineModeQueue[:1]

        predicted_confidence = self.predict_confidence(self.classifier, xt, ypredicted)

        theta = self.config['theta']
        logging.debug(f'refine mode value: cm_y {self.cm[ypredicted]} * '
                      f'conf {predicted_confidence} = {self.cm[ypredicted] * predicted_confidence} <= {theta} theta_q')

        if self.cm[ypredicted] * predicted_confidence <= theta:
            self.refineModeQueue.append(1)
        else:
            self.refineModeQueue.append(0)

    def refine_mode(self):
        theta = self.config['theta'] / 2
        logging.debug('refine mode condition: {} >= {} theta_u ---> {}'.
                      format(float(self.refineModeQueue.count(1)) / len(self.refineModeQueue),
                             theta,
                             float(self.refineModeQueue.count(1)) / len(self.refineModeQueue) >= theta))
        if float(self.refineModeQueue.count(1)) / len(self.refineModeQueue) >= theta:
            return True
        else:
            return False

    def solve_conflict(self, xt, y_predicted, yt):
        if y_predicted == yt:
            logging.debug(f'[conflict] y_predicted {y_predicted} {yt} yt ---> compatible')
            self.cm, self.qm = self.update(self.cm, y_predicted, y_predicted, self.qm)
            self.cu, self.qu = self.update(self.cu, yt, y_predicted, self.qu)

        elif (self.cm[y_predicted] * self.predict_confidence(self.classifier,
                                                             self._get_data_part_from_xt(xt),
                                                             y_predicted)) <= (self.cu[yt] * self.config['theta']):
            predicted_confidence = self.predict_confidence(self.classifier,
                                                           self._get_data_part_from_xt(xt),
                                                           y_predicted)
            logging.debug(f'[conflict] cm_y {self.cm[y_predicted]} * conf {predicted_confidence} = '
                          f'{self.cm[y_predicted] * predicted_confidence} <= {self.cu[yt] * self.config["theta"]} cu_y * theta_c '
                          f'User label taken as ground truth? {self.cm[y_predicted] * predicted_confidence <= self.cu[yt] * self.config["theta"]}')

            self.train_machine(self._get_data_part_from_xt(xt), yt)
            self.cm, self.qm = self.update(self.cm, y_predicted, yt, self.qm)

        else:
            ystar = super().challenge_user(xt, yt, y_predicted)
            logging.debug(f'[conflict] askUserConfirmation {ystar} {y_predicted}. Compatible? {y_predicted == ystar}')
            if not y_predicted == ystar:
                self.train_machine(self._get_data_part_from_xt(xt), ystar)
            self.cm, self.qm = self.update(self.cm, y_predicted, ystar, self.qm)
            self.cu, self.qu = self.update(self.cu, yt, ystar, self.qu)

    def update(self, confidences, y_predicted, yt, q):
        if len(q) == self.config['d']:
            # remove the older
            del q[:1]

        if y_predicted == yt:
            y_predicted = yt

        q.append((y_predicted, yt))

        for label in confidences:
            right_occurrences = self.count_correct_occurrences(q, label)
            total_occurrences = self.count_total_occurrences(q, label)
            if total_occurrences != 0:
                confidences[label] = float(right_occurrences) / total_occurrences

        return confidences, q

    def train_machine(self, x, y) -> None:
        super().add_to_dataset(x, y)
        t0 = time.process_time()
        self.classifier.fit(self.xtrain, self.ytrain)
        t1 = time.process_time()
        self.statistics['training_times'].append(t1 - t0)

    @staticmethod
    def predict_confidence(clf, xt, yt) -> float:
        try:
            proba = clf.predict_proba(xt.values.reshape(1, -1))[0]
            index = np.where(clf.classes_ == yt)[0]
            return proba[index[0]] if index.size != 0 else 0
        except NotFittedError:
            return float(1) / clf.n_classes_ if hasattr(clf, "classes_") else 0

    def iterate_over_test_data(self, test_data):
        y_oracle, y_machine = [], []
        y_machine = list(self.classifier.predict(self._get_data_part_from_xt(test_data)))
        for _, row in test_data.iterrows():
            y_oracle.append(self.get_oracle_label(row))

        return y_oracle, y_machine

    @staticmethod
    def initialize_confidence(labels, value):
        return {label: value for label in labels}

    @staticmethod
    def count_total_occurrences(queue, label) -> int:
        return sum((1 for elem in queue if elem[0] == label or elem[1] == label))

    @staticmethod
    def count_correct_occurrences(queue, label) -> int:
        return queue.count((label, label))

    def calculate_bootstrap(self, ytrain) -> bool:
        return not self.everyone_greater_then(Counter(ytrain).values(), self.config['bootstrap'])

    @staticmethod
    def everyone_greater_then(this_list, n_classes) -> bool:
        return True if len([i for i in this_list if i >= 10]) >= n_classes else False
