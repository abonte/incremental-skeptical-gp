import argparse
import os
import pickle

import numpy as np

from isgp.visualization_all_folds import VisualizationAverage


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("input_path", help="Relative path to results")
parser.add_argument("-name", dest="fig_name", type=str, help="Name of figures", required=False, default=None)
parser.add_argument("-grid", dest='grid', action="store_true", default=False, help="Grid layout")
parser.add_argument("-runtime", dest='runtime', action="store_true", default=False, help="Plot runtime")
args = parser.parse_args()


def main(parameters):
    mean = dict()
    std_error = dict()
    print("Loading...")
    overall_statistics = pickle.load(open(os.path.join(parameters.input_path, "overall_statistics.pickle"), "rb"))
    experiments_args = overall_statistics['args']
    for method_name, statistics in overall_statistics['statistics'].items():
        mean[method_name] = dict()
        std_error[method_name] = dict()
        for metric_name, all_folds_values in statistics.items():
            values_matrix = np.array(all_folds_values)
            if values_matrix.ndim == 2 and metric_name not in ["classifier"]:
                mean[method_name][metric_name] = np.mean(values_matrix, axis=0)
                std_error[method_name][metric_name] = np.std(values_matrix, axis=0) / np.sqrt(values_matrix.shape[0])

    print("Plotting...")
    plots = VisualizationAverage(n_plot_col=2, directory=parameters.input_path, grid=parameters.grid)
    fig_name = args.fig_name if args.fig_name is not None else "all_folds"

    if not parameters.runtime:
        plots.plot_queries_folds(fig_name, mean, std_error, experiments_args)
        plots.plot_f1_score_average(fig_name, mean, std_error, experiments_args)
    else:
        plots.plot_training_predicting_times(mean, std_error)

    plots.show_and_save(fig_name)


if __name__ == '__main__':
    main(args)
