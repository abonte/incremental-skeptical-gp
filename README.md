# Learning in the Wild with Incremental Skeptical Gaussian Processes

Implementation of Incremental Skeptical Gaussian Processes.

See:

Andrea Bontempelli, Stefano Teso, Fausto Giunchiglia and Andrea Passerini. *Learning in the Wild with Incremental Skeptical Gaussian Processes*. Proceedings of the 29th International Joint Conference on Artificial Intelligence (IJCAI'20).

https://doi.org/10.24963/ijcai.2020/399

||||
|---|---|---|
|![isgp](pretty_isgp.png)|![gp never](pretty_skeptical_gp_never.png)|![srf](pretty_skeptical_rf.png)|

[Poster](https://doi.org/10.5281/zenodo.5055508) - [Short video](https://www.ijcai.org/proceedings/2020/video/25387) - [Long video](https://www.ijcai.org/proceedings/2020/video/26961)

## Abstract

The ability to learn from human supervision is fundamental for personal assistants
and other interactive applications of AI. Two central challenges for deploying interactive learners
in the wild are the unreliable nature of the supervision and the varying complexity of the prediction task.
We address a simple but representative setting, incremental classification in the wild,
where the supervision is noisy and the number of classes grows over time. In order to tackle this task,
we propose a redesign of skeptical learning centered around Gaussian Processes (GPs).
Skeptical learning is a recent interactive strategy in which, if the machine is sufficiently confident that
an example is mislabeled, it asks the annotator to reconsider her feedback.
In many cases, this is often enough to obtain clean supervision.
Our redesign, dubbed ISGP, leverages the uncertainty estimates made available by GPs to better allocate
labeling and contradiction queries, especially in the presence of noise.
Our experiments on synthetic and real-world data show that, as a result, while the original
formulation of skeptical learning produces overconfident models that can fail completely in the wild,
ISGP works well at varying levels of noise and as new classes are observed.

## Getting started

### Prerequisites

- python >= 3.7
- pandas >= 1.0.2
- scikit-learn >= 0.22.1
- matplotlib
- tqdm

Older versions may also work.

### Installing

```
conda env create -f environment.yml
conda activate 2020_isgp
```

### Example

Example of workflow:

1) create 6 clusters, each with 100 samples (modify `make_dataset.py` if you want to change the position of the centroids,
number of clusters or the standard deviation):
    ```bash
    python make_dataset.py sequential_cluster -k 10 --seed 0 -n 100
    ```
2) run the experiments
    ```bash
    python run.py experiment_sequential_cluster -04 -no-oracle -theta 0.2 -i data/sequential_cluster/ -v -n 10
    
    python draw.py results/experiment_sequential_cluster -grid
    ```

### Create dataset

```
usage: make_dataset.py [-h] [-k N_SPLITS] [-train-size TRAIN_SIZE]
                       [-test-size TRAINTEST] [-shuffle] [-s SEED]
                       [-n SAMPLE_PER_CLUSTER]
                       dataset_name

positional arguments:
  dataset_name

optional arguments:
  -h, --help            show this help message and exit
  -k N_SPLITS, --n-splits N_SPLITS
                        Create folds. Number of cross-validation folds
                        (default: 10)
  -train-size TRAIN_SIZE
                        In case of kfols, this is the number of samples in the
                        k-1 folds used for training (default: None)
  -test-size TRAINTEST  Create a training and testing set. Float between 0.0
                        and 1.0 (default: None)
  -shuffle              Shuffle samples in training set (default: False)
  -s SEED, --seed SEED  RNG seed (default: 0)
  -n SAMPLE_PER_CLUSTER
                        Number of sample in each cluster (default: 100)
```

### Run experiments
```
usage: run.py [-h] (-04 | -01) (-oracle | -no-oracle) -i INPUT_FOLDER
              [-theta THETA] [-s SEED] [-n N_PROCESSES] [-v] [-R RHO] [-t]
              [--n-tree N_TREE] [--force-stages]
              experiment_name

positional arguments:
  experiment_name

optional arguments:
  -h, --help            show this help message and exit
  -04                   Dataset with 40% noise (default: None)
  -01                   Dataset with 10% noise (default: None)
  -oracle               When the user is contradicted, she returns the ground
                        truth label (default: False)
  -no-oracle            When the user is contradicted, she may return the
                        ground truth label (default: True)
  -i INPUT_FOLDER, --input INPUT_FOLDER
                        Path to the input folder (default: None)
  -theta THETA
  -s SEED, --seed SEED  RNG seed (default: 0)
  -n N_PROCESSES, --n-processes N_PROCESSES
                        Number of worker processes to use (default: 8)
  -v, --verbose
  -R RHO, --rho RHO     Smoothing parameter that models noise in Gaussian
                        Processes (default: 1e-10)
  -t, --runtime         Measure runtime (default: False)
  --n-tree N_TREE       Number of trees of Random Forest (default: 100)
  --force-stages
```

### Generate plots

```
usage: draw.py [-h] [-name FIG_NAME] [-grid] [-runtime] input_path

positional arguments:
  input_path      Relative path to results

optional arguments:
  -h, --help      show this help message and exit
  -name FIG_NAME  Name of figures (default: None)
  -grid           Grid layout (default: False)
  -runtime        Plot runtime (default: False)
```

## Structure 

|   |   |
|---|---|
| `isgp/gaussian_process/`| Implementation of incremental GP *[Lütz et al., 2013]*|
|`isgp/isgp.py`   |  Incremental skeptical Gaussian Processes |
|`isgp/original_skl.py`   |  Original formulation of skeptical learning *[Zeni et al., 2019]*  |
|`data/10folds_sequential`| Sequential clusters|
|`data/10folds_shuffle`| Data points randomly mixed|

## Reproducibility of the experiments

### Figure 2

Run the following command with the parameters in the table.

```bash
python run.py
```

|noise 10%  |noise 40%   |
|---|---|
|`shuffle_same_f1 -01 -no-oracle -theta 0.1 -i "data/10folds_shuffle/"`|`shuffle_same_f1 -04 -no-oracle -theta 0.22 -i "data/10folds_shuffle/"`|
|`sequential_same_f1 -01 -no-oracle -theta 0.55 -i "data/10folds_sequential/"`|`sequential_same_f1 -04 -no-oracle -theta 0.3 -i "data/10folds_sequential/"`|
|`shuffle_same_queries -01 -no-oracle -theta 0.11 --force-stages -i "data/10folds_shuffle/"`|`shuffle_same_queries -04 -no-oracle -theta 0.05 --force-stages -i "data/10folds_shuffle/"`|
|`sequential_same_queries -01 -no-oracle -theta 0.1 --force-stages -i "data/10folds_sequential/"`|`sequential_same_queries -04 -no-oracle -theta 0.15 --force-stages -i "data/10folds_sequential/"`|

To generate the plots, call `draw.py` with the respective folder name:

```bash
python draw.py results/<folder_name> -grid
```

### Figure 3

At the moment the real-world data are not available due to privacy reasons.

Last plot:

```bash
python run.py runtime -04 -no-oracle --runtime -i "data/10folds_sequential/"
python draw.py results/<folder_name> -grid -runtime
```

## Cite

```markdown
@inproceedings{bontempelli2020learning,
  title={Learning in the Wild with Incremental Skeptical Gaussian Processes},
  author={Bontempelli, Andrea and Teso, Stefano and Giunchiglia, Fausto and Passerini, Andrea},
  booktitle={Proceedings of the 29th International Joint Conference on Artificial Intelligence (IJCAI)},
  pages={2886--2892},
  year={2020},
  doi={10.24963/ijcai.2020/399}
} 
```

## Acknowledgments

The algorithm was substantially improved thanks to the input of the anonymous
referees. The research has received funding from the European Union's Horizon 2020 FET Proactive project 
"WeNet -- The Internet of us", grant agreement No 823783 and from the "DELPhi - DiscovEring Life Patterns" project
funded by the MIUR Progetti di Ricerca di Rilevante Interesse Nazionale (PRIN) 2017 -- DD n. 1062 del 31.05.2019.


## References

- **[Zeni et al., 2019]** Mattia Zeni, Wanyi Zhang, Enrico Bignotti, Andrea Passerini, and Fausto Giunchiglia.
Fixing mislabeling by human annotators leveraging conflict resolution and prior knowledge. IMWUT, 2019.
- **[Lütz et al., 2013]** Alexander Lü̈tz, Erik Rodner, and Joachim Denzler.
I Want To Know More – Efficient Multi-Class Incremental Learning Using Gaussian Processes. Pattern Recognition and Image Analysis, 2013.

