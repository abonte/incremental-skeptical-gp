import argparse
import os

import numpy as np
import pandas as pd
from sklearn.datasets import make_blobs
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split


def generate_dataset(datapoints_per_cluster, centers, cluster_std, seed):
    n_samples = datapoints_per_cluster * len(centers)
    X, y = make_blobs(n_samples=n_samples, centers=centers, cluster_std=cluster_std, shuffle=False,
                      random_state=seed)
    dataset = pd.DataFrame(data=X.reshape(-1, 2), columns=['x', 'y'])
    dataset.insert(0, column="oracle", value=y)
    return dataset


def add_noise_to_data(dataset, noise_percentage, seed):
    df = dataset.copy()
    df.insert(1, column=str(noise_percentage), value=df["oracle"])
    sample_dataset = df.sample(frac=noise_percentage,
                               random_state=int(seed + noise_percentage * 10))
    classes = np.arange(len(np.unique(dataset["oracle"])))
    for index, row in sample_dataset.iterrows():
        sample_dataset.at[index, str(noise_percentage)] = np.random.choice(
            classes[classes != sample_dataset.loc[index]['oracle']])
    df.at[sample_dataset.index, str(noise_percentage)] = sample_dataset[str(noise_percentage)]
    return df


def _create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)


def main():
    config = {
        "centers": [[-5, 0], [5, 0], [2, 4], [-3, -4], [-2, 4], [2, -4]],
        "cluster_std": 1.5,
        "noise_levels": [.1, .2, .3, .4],
        "base_path": "data/",
    }

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("dataset_name")
    parser.add_argument('-k', '--n-splits', type=int, default=10, help='Create folds. Number of cross-validation folds')
    parser.add_argument('-train-size', type=int, help='In case of kfols, this is the number of samples in the k-1 folds used for training')
    parser.add_argument("-test-size", type=float, dest='traintest', help="Create a training and testing set. Float between 0.0 and 1.0")
    parser.add_argument("-shuffle", dest='shuffle', action="store_true", default=False, help="Shuffle samples in training set")
    parser.add_argument('-s', '--seed', type=int, default=0, help='RNG seed')
    parser.add_argument('-n', dest="sample_per_cluster", type=int, default=100, help='Number of sample in each cluster')
    args = parser.parse_args()
    config["shuffle"] = args.shuffle

    print("Creating dataset...")
    dataset = generate_dataset(args.sample_per_cluster, config["centers"], cluster_std=config["cluster_std"],
                               seed=args.seed)
    for noise in config["noise_levels"]:
        dataset = add_noise_to_data(dataset, noise, seed=0)

    path = None
    if args.n_splits:
        config["kfolds"] = args.n_splits
        path = os.path.join(config["base_path"], str(args.n_splits) + args.dataset_name)
        _create_folder(path)
        skf = StratifiedKFold(n_splits=args.n_splits, shuffle=True, random_state=args.seed)
        for n_fold, (train_index, test_index) in enumerate(skf.split(dataset, dataset["oracle"])):
            train = dataset.iloc[train_index].copy()
            if args.train_size:
                train, _ = train_test_split(train,
                                            train_size=args.train_size,
                                            shuffle=True,
                                            random_state=args.seed,
                                            stratify=train['oracle'])

            if args.shuffle:
                train = train.sample(frac=1, random_state=args.seed)
            else:
                train.sort_values(by=['oracle'], inplace=True)
            train.to_csv(os.path.join(path, str(n_fold) + "_trainset.csv"))
            dataset.iloc[test_index].to_csv(os.path.join(path, str(n_fold) + "_testset.csv"))
    elif args.test_size:
        config["test_size"] = args.traintest
        train_data, test_data = train_test_split(dataset,
                                                 shuffle=True,
                                                 test_size=args.traintest,
                                                 stratify=dataset["oracle"],
                                                 random_state=args.seed)
        if not args.shuffle:
            train_data.sort_values(by=['oracle'], inplace=True)

        path = os.path.join(config["base_path"], args.dataset_name)
        _create_folder(path)
        train_data.to_csv(os.path.join(path, "trainset.csv"))
        test_data.to_csv(os.path.join(path, "testset.csv"))

    if path is not None:
        with open(path + 'config.txt', 'w') as f:
            for key in config.keys():
                f.write("%s:%s\n" % (key, config[key]))

    print("Done")


if __name__ == '__main__':
    main()
