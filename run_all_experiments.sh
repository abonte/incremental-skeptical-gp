nprocess=8
seed=0

echo "=== Shuffle same f1 0.1===\n"
python run.py shuffle_same_f1 -01 -no-oracle -theta 0.1 -i "data/10folds_shuffle/" -n $nprocess -s $seed
echo "=== Shuffle same f1 0.4===\n"
python run.py shuffle_same_f1 -04 -no-oracle -theta 0.22 -i "data/10folds_shuffle/" -n $nprocess -s $seed
echo "=== Shuffle same queries 0.1===\n"
python run.py shuffle_same_queries -01 -no-oracle -theta 0.11 --force-stages -i "data/10folds_shuffle/" -n $nprocess -s $seed
echo "=== Shuffle same queries 0.4===\n"
python run.py shuffle_same_queries -04 -no-oracle -theta 0.05 --force-stages -i "data/10folds_shuffle/" -n $nprocess -s $seed

echo "=== Sequential same f1 0.1===\n"
python run.py sequential_same_f1 -01 -no-oracle -theta 0.55 -i "data/10folds_sequential/" -n $nprocess -s $seed
echo "=== Sequential same f1 0.4===\n"
python run.py sequential_same_f1 -04 -no-oracle -theta 0.3 -i "data/10folds_sequential/" -n $nprocess -s $seed
echo "=== Sequential same queries 0.1===\n"
python run.py sequential_same_queries -01 -no-oracle -theta 0.1 --force-stages -i "data/10folds_sequential/" -n $nprocess -s $seed
echo "=== Sequential same queries 0.4 ===\n"
python run.py sequential_same_queries -04 -no-oracle -theta 0.15 --force-stages -i "data/10folds_sequential/" -n $nprocess -s $seed

python run.py runtime -04 -no-oracle --runtime -i "data/10folds_sequential/" -n $nprocess -s 0