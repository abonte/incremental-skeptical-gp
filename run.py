#!/usr/bin/env python
# coding: utf-8
import argparse
import copy
import logging
import multiprocessing as mp
import os
import pickle
import time
from collections import defaultdict

import pandas as pd
from sklearn.gaussian_process.kernels import RBF
from tqdm import tqdm

from isgp.isgp import ISGP, ContradictionType
from isgp.original_skl import OriginalSKL
from isgp.visualization_one_fold import Visualization


def run_for_single_dataset(args, training_data, test_data, n_fold: int):
    configs = _get_configurations(args)

    classifiers = [
        ("ISGP", ISGP(configs['ISGP'])),
        ("SRF", OriginalSKL(configs['SRF']))
    ]

    if not args.runtime:
        classifiers = [
            ("GP_never", ISGP(configs['GP_never'])),
            ("GP_always", ISGP(configs['GP_always'])),
            ("ISGP", ISGP(configs['ISGP'])),
            ("SRF", OriginalSKL(configs['SRF']))
        ]

    statistics_all_clf = {}
    time.sleep(n_fold / 100)
    progress_bar = tqdm(classifiers, position=n_fold)
    for name, clf in progress_bar:
        progress_bar.set_description(f'Processing {name} in fold n. {n_fold}...')
        statistics_all_clf[name] = clf.run(training_data, test_data)

    return statistics_all_clf


def _get_configurations(args):
    noise_level = args.noise if not args.oracle else 0
    original_skl_config = {
        "theta": args.theta,
        "random_state": args.seed,
        "n_tree": args.n_tree,
        "d": 1000,
        "noise_level": noise_level,
        "verbose": args.verbose,
        "measure_run_time": args.runtime,
        "force_stage": args.force_stages,
        "noise": args.noise,
        "bootstrap": 2
    }
    gp_config = {
        "random_state": args.seed,
        "noise_level": noise_level,
        "kernel": 1.0 * RBF(2),
        "sigma_squared": args.rho,
        "verbose": args.verbose,
        "contradiction_condition": None,
        "measure_run_time": args.runtime,
        "noise": args.noise
    }
    gp_never, gp_always, isgp = copy.deepcopy(gp_config), copy.deepcopy(gp_config), copy.deepcopy(gp_config)
    gp_never["contradiction_condition"] = ContradictionType.NEVER
    gp_always['contradiction_condition'] = ContradictionType.ALWAYS
    isgp['contradiction_condition'] = ContradictionType.STOCHASTIC

    config = {
        "GP_never": gp_never,
        "GP_always": gp_always,
        "ISGP": isgp,
        "SRF": original_skl_config
    }
    return config


def plot_all(statistics, training_set, test_set, output_folder: str, noise: float, nfold: int):
    plots = Visualization(n_plot_col=8,
                          dataset=training_set,
                          grid=True,
                          directory=output_folder,
                          timestamp=None)

    for classifier_name, stat in statistics.items():
        plots.contour_plot(stat, training_set, test_set, noise, classifier_name)

    plots.plot_input_data(training_set, noise)
    plots.plot_queries(statistics)
    plots.plot_f1_score(statistics)

    plots.show_and_save(str(nfold) + '_fold')


def run_for_each_kfold_iteration(args, n_fold: int, output_folder: str):
    logging.info(f'N_fold: {n_fold}')

    training_set, test_set = _load_train_test_set(f'{args.input_folder}{n_fold}_')

    statistics = run_for_single_dataset(args, training_set, test_set, n_fold)

    if not args.runtime:
        plot_all(statistics, training_set, test_set, output_folder, args.noise, n_fold)

    dump(os.path.join(output_folder, f'{n_fold}_fold_statistics.pickle'), {
        'args': args,
        'statistics': statistics,
        'training_set': training_set
    })

    return statistics


def _load_train_test_set(path: str):
    train = pd.read_csv(path + "trainset.csv", index_col=0)
    test = pd.read_csv(path + "testset.csv", index_col=0)
    return train, test


def _get_basename(args) -> str:
    fields = [
        ('noise', args.noise),
        ('oracle', args.oracle),
        ('s', args.seed),
        ('theta', args.theta),
        ('rho', args.rho),
        ('ntree', args.n_tree)
    ]

    basename = args.experiment_name + '__' + '__'.join([name + '=' + str(value)
                                                        for name, value in fields])
    return basename


def dump(path, what, **kwargs):
    with open(path, 'wb') as fp:
        pickle.dump(what, fp, **kwargs)


def merge(stat_all_fold: list):
    metrics = dict()
    for i, elem in enumerate(stat_all_fold):
        for clf_name, statistics in elem.items():
            if i == 0:
                metrics[clf_name] = defaultdict(list)
            for k, v in statistics.items():
                metrics[clf_name][k].append(v)
    return metrics


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("experiment_name")
    group_noise = parser.add_mutually_exclusive_group(required=True)
    group_noise.add_argument("-04", dest='noise', const=0.4, action='store_const', help="Dataset with 40%% noise")
    group_noise.add_argument("-01", dest='noise', const=0.1, action='store_const', help="Dataset with 10%% noise")
    group_oracle = parser.add_mutually_exclusive_group(required=True)
    group_oracle.add_argument("-oracle", dest='oracle', action="store_true",
                              help="When the user is contradicted, she returns the ground truth label")
    group_oracle.add_argument("-no-oracle", dest='oracle', action="store_false",
                              help="When the user is contradicted, she may return the ground truth label")
    parser.add_argument('-i', '--input', dest='input_folder', type=str, help="Path to the input folder", required=True)
    parser.add_argument("-theta", type=float, dest='theta', default=None)
    parser.add_argument('-s', '--seed', type=int, default=0, help='RNG seed')
    parser.add_argument('-n', '--n-processes', type=int, default=mp.cpu_count(),
                        help='Number of worker processes to use')
    parser.add_argument('-v', '--verbose', action="store_true", default=False)
    parser.add_argument('-R', '--rho', type=float, default=1e-10,
                        help='Smoothing parameter that models noise in Gaussian Processes')
    parser.add_argument('-t', '--runtime', action="store_true", default=False, help='Measure runtime')
    parser.add_argument('--n-tree', type=int, default=100, help='Number of trees of Random Forest')
    parser.add_argument('--force-stages', action="store_true", default=False)
    args = parser.parse_args()

    if not args.verbose:
        logging.basicConfig(level=logging.WARN)
    output_folder = os.path.join("results", _get_basename(args))
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    n_splits = len([name for name in os.listdir(args.input_folder)
                    if os.path.isfile(os.path.join(args.input_folder, name)) and "train" in name])

    pool = mp.Pool(args.n_processes)
    statistics_all_folds = pool.starmap_async(run_for_each_kfold_iteration,
                                              [(args, n_fold, output_folder)
                                               for n_fold in range(n_splits)]).get()
    pool.close()
    pool.join()

    dump(os.path.join(output_folder, 'overall_statistics.pickle'), {
        'args': args,
        'statistics': merge(statistics_all_folds)
    })


if __name__ == "__main__":
    main()
